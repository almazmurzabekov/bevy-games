use bevy::prelude::*;
use crate::game::{ARENA_HEIGHT, ARENA_WIDTH};
use crate::game::food::Food;
use crate::game::structs::{Position, Size};
use crate::menu::GameState;

const SNAKE_HEAD_COLOR: Color = Color::WHITE;
const SNAKE_SEGMENT_COLOR: Color = Color::rgb(0.3, 0.3, 0.3);

#[derive(PartialEq, Eq, Copy, Clone)]
pub enum MovementDirection {
    Left,
    Right,
    Up,
    Down,
}

impl MovementDirection {
    pub fn opposite(&self) -> Self {
        match self {
            MovementDirection::Left => Self::Right,
            MovementDirection::Right => Self::Left,
            MovementDirection::Up => Self::Down,
            MovementDirection::Down => Self::Up,
        }
    }
}

pub struct GrowthEvent;

pub struct GameOverEvent;

#[derive(Component)]
pub struct SnakeSegment;

#[derive(Default, Deref, DerefMut, Resource)]
pub struct SnakeSegments(Vec<Entity>);

#[derive(Default, Resource)]
pub struct LastTailPosition(Option<Position>);

#[derive(Component)]
pub struct SnakeHead {
    direction: MovementDirection,
}

pub fn spawn_segment(mut commands: Commands, position: Position) -> Entity {
    commands
        .spawn(SpriteBundle {
            sprite: Sprite {
                color: SNAKE_SEGMENT_COLOR,
                ..default()
            },
            ..default()
        })
        .insert(SnakeSegment)
        .insert(position)
        .insert(Size::square(0.65))
        .id()
}

pub fn spawn_snake(mut commands: Commands, mut segments: ResMut<SnakeSegments>) {
    *segments = SnakeSegments(vec![
        commands
            .spawn(SpriteBundle {
                sprite: Sprite {
                    color: SNAKE_HEAD_COLOR,
                    ..default()
                },
                ..default()
            })
            .insert(SnakeHead {
                direction: MovementDirection::Up,
            })
            .insert(SnakeSegment)
            .insert(Position { x: 3, y: 3 })
            .insert(Size::square(0.8))
            .id(),
        spawn_segment(commands, Position { x: 3, y: 2 }),
    ]);
}

pub fn snake_movement(
    segments: ResMut<SnakeSegments>,
    mut heads: Query<(Entity, &SnakeHead)>,
    mut tail_position: ResMut<LastTailPosition>,
    mut positions: Query<&mut Position>,
    mut game_over_writer: EventWriter<GameOverEvent>,
) {
    if let Some((head_entity, head)) = heads.iter_mut().next() {
        let segment_positions = segments
            .iter()
            .map(|e| *positions.get_mut(*e).unwrap())
            .collect::<Vec<Position>>();

        let mut head_position = positions.get_mut(head_entity).unwrap();

        match head.direction {
            MovementDirection::Left => head_position.x -= 1,
            MovementDirection::Right => head_position.x += 1,
            MovementDirection::Up => head_position.y += 1,
            MovementDirection::Down => head_position.y -= 1,
        };

        if head_position.x < 0
            || head_position.y < 0
            || head_position.x as u32 >= ARENA_WIDTH
            || head_position.y as u32 >= ARENA_HEIGHT {
            game_over_writer.send(GameOverEvent);
        }

        if segment_positions.contains(&head_position) {
            game_over_writer.send(GameOverEvent);
        }

        segment_positions
            .iter()
            .zip(segments.iter().skip(1))
            .for_each(|(pos, segment)| *positions.get_mut(*segment).unwrap() = *pos);
        *tail_position = LastTailPosition(Some(*segment_positions.last().unwrap()))
    }
}

pub fn snake_movement_input(
    keyboard_input: Res<Input<KeyCode>>,
    mut heads: Query<&mut SnakeHead>,
    mut menu_state: ResMut<State<GameState>>
) {
    for mut head in heads.iter_mut() {
        let new_direction = match keyboard_input.get_pressed().next() {
            Some(KeyCode::Left) => MovementDirection::Left,
            Some(KeyCode::Right) => MovementDirection::Right,
            Some(KeyCode::Up) => MovementDirection::Up,
            Some(KeyCode::Down) => MovementDirection::Down,
            Some(KeyCode::Escape) => {
                let _ = menu_state.set(GameState::Menu);
                head.direction
            },
            _ => head.direction,
        };

        if head.direction.opposite() != new_direction {
            head.direction = new_direction
        }
    }
}

pub fn snake_eating(
    mut commands: Commands,
    mut growth_writer: EventWriter<GrowthEvent>,
    food_positions: Query<(Entity, &Position), With<Food>>,
    head_positions: Query<&Position, With<SnakeHead>>,
) {
    for head_pos in head_positions.iter() {
        for (entity, food_pos) in food_positions.iter() {
            if head_pos == food_pos {
                commands.entity(entity).despawn();
                growth_writer.send(GrowthEvent)
            }
        }
    }
}

pub fn snake_growth(
    commands: Commands,
    tile_position: ResMut<LastTailPosition>,
    mut segments: ResMut<SnakeSegments>,
    mut growth_reader: EventReader<GrowthEvent>,
) {
    if growth_reader.iter().next().is_some() {
        segments.push(spawn_segment(commands, tile_position.0.unwrap()))
    }
}

pub fn game_over(
    mut commands: Commands,
    mut game_over_reader: EventReader<GameOverEvent>,
    segments_res: ResMut<SnakeSegments>,
    foods: Query<Entity, With<Food>>,
    segments: Query<Entity, With<SnakeSegment>>)
{
    if game_over_reader.iter().next().is_some() {
        for ent in foods.iter().chain(segments.iter()) {
            commands.entity(ent).despawn();
        }
        spawn_snake(commands, segments_res)
    }
}

pub fn size_scaling(windows: Res<Windows>, mut q: Query<(&Size, &mut Transform)>) {
    if let Some(window) = windows.get_primary() {
        for (size, mut transform) in q.iter_mut() {
            transform.scale = Vec3::new(
                size.width / ARENA_WIDTH as f32 * window.width(),
                size.height / ARENA_WIDTH as f32 * window.height(),
                1.0,
            )
        }
    }
}

pub fn position_translation(
    windows: Res<Windows>,
    mut q: Query<(&Position, &mut Transform)>,
) {
    fn convert(pos: f32, bound_window: f32, bound_game: f32) -> f32 {
        let tile_size = bound_window / bound_game;
        pos / bound_game * bound_window - (bound_window / 2.) + (tile_size / 2.)
    }

    if let Some(window) = windows.get_primary() {
        for (position, mut transform) in q.iter_mut() {
            transform.translation = Vec3::new(
                convert(position.x as f32, window.width() as f32, ARENA_WIDTH as f32),
                convert(
                    position.y as f32,
                    window.height() as f32,
                    ARENA_HEIGHT as f32,
                ),
                0.0,
            )
        }
    }
}
