use bevy::prelude::*;
use rand::prelude::random;
use crate::game::{ARENA_HEIGHT, ARENA_WIDTH};
use crate::game::snake::SnakeSegment;
use crate::game::structs::{Position, Size};

const FOOD_COLOR: Color = Color::RED;
const MAX_FOOD_COUNT: usize = 5;


#[derive(Component)]
pub struct Food;

pub fn food_spawner(mut commands: Commands,
                           foods: Query<Entity, With<Food>>,
                           segments: Query<&Position, With<SnakeSegment>>) {
    if foods.iter().len() <= MAX_FOOD_COUNT {
        loop {
            let x = (random::<f32>() * ARENA_WIDTH as f32) as i32;
            let y = (random::<f32>() * ARENA_HEIGHT as f32) as i32;

            if segments.iter().find(|s| s.x == x && s.y == y).is_some(){
                continue;
            }

            commands
                .spawn(SpriteBundle {
                    sprite: Sprite {
                        color: FOOD_COLOR,
                        ..default()
                    },
                    ..default()
                })
                .insert(Food)
                .insert(Position { x, y })
                .insert(Size::square(0.8));
            return;
        }
    }
}
