// use bevy::diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin};
use bevy::prelude::*;

use crate::menu::{DisplayQuality, GameState, Volume};
use crate::menu::plugins::{MenuPlugin, SplashPlugin, GamePlugin};

pub mod snake;
pub mod food;
pub mod structs;

pub const WINDOW_WIDTH: f32 = 500.;
pub const WINDOW_HEIGHT: f32 = 500.;

pub const ARENA_WIDTH: u32 = 20;
pub const ARENA_HEIGHT: u32 = 20;

fn setup_camera(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}

pub fn start() {
    App::new()
        // Insert as resource the initial value for the settings resources
        .insert_resource(DisplayQuality::Medium)
        .insert_resource(Volume(7))
        .insert_resource(ClearColor(Color::BLACK))
        // Declare the game state, and set its startup value
        .add_state(GameState::Splash)
        .add_startup_system(setup_camera)
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            window: WindowDescriptor {
                title: "Snake game".to_string(),
                width: WINDOW_WIDTH,
                height: WINDOW_HEIGHT,
                ..default()
            },
            ..default()
        }))
        // Adds the plugins for each state
        .add_plugin(SplashPlugin)
        .add_plugin(MenuPlugin)
        .add_plugin(GamePlugin)
        // logging plugins
        // .add_plugin(LogDiagnosticsPlugin::default())
        // .add_plugin(FrameTimeDiagnosticsPlugin::default())
        .run();
}
