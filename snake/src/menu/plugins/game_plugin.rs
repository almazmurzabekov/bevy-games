use bevy::prelude::*;
use bevy::time::FixedTimestep;

use crate::game::food::food_spawner;
use crate::game::snake::*;
use crate::menu::{despawn_screen, GameState};

pub struct GamePlugin;

impl Plugin for GamePlugin {
    fn build(&self, app: &mut App) {
        app
            .add_system_set(SystemSet::on_enter(GameState::Game).with_system(spawn_snake))
            .add_system_set(SystemSet::on_update(GameState::Game)
                .with_system(snake_movement_input.before(snake_movement))
                .with_system(game_over.after(snake_movement))
            )
            .add_system_set(
                SystemSet::on_update(GameState::Game)
                    .with_run_criteria(FixedTimestep::step(0.25))
                    .with_system(snake_movement)
                    .with_system(snake_eating.after(snake_movement))
                    .with_system(snake_growth.after(snake_eating))
            )
            .add_system_set(
                SystemSet::on_update(GameState::Game)
                    .with_run_criteria(FixedTimestep::step(1.0))
                    .with_system(food_spawner),
            )
            .add_system_set_to_stage(
                CoreStage::PostUpdate,
                SystemSet::new()
                    .with_system(position_translation)
                    .with_system(size_scaling),
            )
            // .add_startup_system(spawn_snake)
            .insert_resource(SnakeSegments::default())
            .insert_resource(LastTailPosition::default())
            .add_event::<GrowthEvent>()
            .add_event::<GameOverEvent>()
            .add_system_set(
                SystemSet::on_exit(GameState::Game).with_system(despawn_screen::<OnGameScreen>),
            );
    }
}

// Tag component used to tag entities added on the game screen
#[derive(Component)]
struct OnGameScreen;
