use bevy::prelude::*;
use bevy::window::PresentMode;
use crate::game::GamePlugin;
use crate::menu::MenuPlugin;
use crate::structs::{GameState, WindowSize};

mod menu;
mod structs;
mod game;
mod constants;
mod load_splash;

use constants::*;
use crate::load_splash::SplashPlugin;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(
            WindowPlugin {
                window: WindowDescriptor {
                    title: "Main menu example".to_string(),
                    width: WINDOW_WIDTH,
                    height: WINDOW_HEIGHT,
                    present_mode: PresentMode::AutoVsync,
                    resizable: false,
                    ..Default::default()
                },
                ..Default::default()
            }
        ))
        .add_state::<GameState>(GameState::default())
        .add_startup_system(setup)
        .add_plugin(MenuPlugin)
        .add_plugin(GamePlugin)
        .add_plugin(SplashPlugin)
        .run()
}

fn setup(mut commands: Commands, mut windows: ResMut<Windows>) {
    commands.spawn(Camera2dBundle::default());

    if let Some(window) = windows.get_primary_mut() {
        let (width, height) = (window.width(), window.height());
        window.set_position(MonitorSelection::Current, IVec2::new(1520, 0));

        commands.insert_resource(WindowSize::new(width, height));
    }
}

pub fn despawn_screen<T: Component>(mut commands: Commands, to_despawn: Query<Entity, With<T>>) {
    for entity in to_despawn.iter() {
        commands.entity(entity).despawn();
    }
}
