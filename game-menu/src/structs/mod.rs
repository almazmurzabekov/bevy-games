use bevy::prelude::*;

#[derive(Resource)]
pub struct WindowSize {
    pub width: f32,
    pub height: f32,
}

impl WindowSize {
    pub fn new(width: f32, height: f32) -> Self {
        Self {width, height}
    }
}

#[derive(Clone, Copy, Default, Eq, PartialEq, Debug, Hash)]
pub enum GameState {
    #[default]
    InitialLoading,
    ShowingMenu,
    PlayingGame,
    LoadingLevel,
}

#[derive(Clone, Copy, Default, Eq, PartialEq, Debug, Hash)]
pub enum MenuState {
    #[default]
    Main,
    Settings,
    Disabled
}




