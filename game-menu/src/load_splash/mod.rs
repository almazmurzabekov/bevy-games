use std::time::Duration;
use bevy::prelude::*;
use bevy::app::{App, Plugin};
use crate::constants::{LOADING_BACKGROUND_SPRITE, LOADING_BACKGROUND_SPRITE_SIZE};
use crate::despawn_screen;
use crate::structs::{GameState, WindowSize};

pub struct SplashPlugin;

impl Plugin for SplashPlugin {
    fn build(&self, app: &mut App) {
        app
            .add_system_set(SystemSet::on_enter(GameState::InitialLoading)
                .with_system(init_initial_loading_screen))
            .add_system_set(SystemSet::on_update(GameState::InitialLoading)
                .with_system(initial_loading_progress))
            .add_system_set(SystemSet::on_exit(GameState::InitialLoading)
                .with_system(despawn_screen::<OnSplashScreen>));
    }
}

#[derive(Component)]
pub struct OnSplashScreen;

#[derive(Resource)]
pub struct SplashTimer(Timer);

fn init_initial_loading_screen(
    mut commands: Commands,
    window_size: Res<WindowSize>,
    asset_loader: Res<AssetServer>,
) {
    let handle: Handle<Image> = asset_loader.load(LOADING_BACKGROUND_SPRITE);

    let (sprite_w, sprite_h) = LOADING_BACKGROUND_SPRITE_SIZE;
    let scale_x = window_size.width / sprite_w;
    let scale_y = window_size.height / sprite_h;

    commands
        .spawn(SpriteBundle {
            transform: Transform {
                scale: Vec3::new(scale_x, scale_y, 1.),
                ..Default::default()
            },
            texture: handle.clone(),
            ..Default::default()
        })
        .insert(OnSplashScreen);

    commands.insert_resource(SplashTimer(Timer::new(Duration::from_secs(1), TimerMode::Once)));
}

fn initial_loading_progress(
    mut game_state: ResMut<State<GameState>>,
    time: Res<Time>,
    mut timer: ResMut<SplashTimer>,
) {
    if timer.0.tick(time.delta()).finished() {
        game_state.set(GameState::ShowingMenu).unwrap();
    } else {
        // TODO: Lading Spinner
    }
}