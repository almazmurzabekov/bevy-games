use bevy::prelude::*;


mod menu_plugin;
pub use menu_plugin::MenuPlugin;

#[derive(Resource, Default)]
pub struct MainMenuAssets {
    pub background_image: Handle<Image>,
    pub default_button_image: Handle<Image>,
    pub default_button_image_pressed: Handle<Image>,
    pub default_button_image_hovered: Handle<Image>,
    pub selected_button_image: Handle<Image>,
    pub selected_button_image_pressed: Handle<Image>,
    pub selected_button_image_hovered: Handle<Image>,
    pub ok_button_background: Handle<Image>,
    pub cancel_button_background: Handle<Image>,

    pub main_font: Handle<Font>,


}