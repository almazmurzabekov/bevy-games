use bevy::prelude::*;
use bevy::app::StartupStage::PostStartup;
use bevy::asset::AssetServer;
use bevy::math::Vec3;
use bevy::prelude::{Color, Commands, Component, NodeBundle, Res, SpriteBundle, SystemSet, Transform};
use bevy::render::render_resource::BindingType::Texture;
use bevy::ui::*;
use bevy::window::Windows;
use crate::constants::{BACKGROUND_SPRITE, BACKGROUND_SPRITE_SIZE, MAIN_FONT, BUTTON_TEXT_COLOR, MAIN_BUTTON, MAIN_BUTTON_PRESSED, SELECTED_BUTTON, SELECTED_BUTTON_PRESSED, MAIN_BUTTON_HOVERED, SELECTED_BUTTON_HOVERED};
use crate::despawn_screen;
use crate::menu::MainMenuAssets;
use crate::structs::{GameState, MenuState, WindowSize};

pub struct MenuPlugin;

impl Plugin for MenuPlugin {
    fn build(&self, app: &mut App) {
        app
            .add_startup_system_to_stage(PostStartup, load_menu_assets)
            .add_system_set(SystemSet::on_enter(GameState::ShowingMenu)
                .with_system(init_main_menu_screen))
            .add_system_set(SystemSet::on_update(GameState::ShowingMenu)
                .with_system(main_menu_screen_update))
            .add_system_set(SystemSet::on_exit(GameState::ShowingMenu)
                .with_system(despawn_screen::<OnMainMenuScreen>));
    }
}

#[derive(Component)]
pub enum MenuButton {
    StartGame,
    Settings,
    Exit
}

#[derive(Component)]
pub struct OnMainMenuScreen;

fn load_menu_assets(mut commands: Commands, asset_loader: Res<AssetServer>) {
    let menu_sprites = MainMenuAssets {
        background_image: asset_loader.load(BACKGROUND_SPRITE),
        default_button_image: asset_loader.load(MAIN_BUTTON),
        default_button_image_pressed: asset_loader.load(MAIN_BUTTON_PRESSED),
        default_button_image_hovered: asset_loader.load(MAIN_BUTTON_HOVERED),
        selected_button_image: asset_loader.load(SELECTED_BUTTON_PRESSED),
        selected_button_image_pressed: asset_loader.load(SELECTED_BUTTON),
        selected_button_image_hovered: asset_loader.load(SELECTED_BUTTON_HOVERED),
        main_font: asset_loader.load(MAIN_FONT),
        ..Default::default()
    };
    commands.insert_resource(menu_sprites);
}

fn init_main_menu_screen(
    mut commands: Commands,
    window: Res<WindowSize>,
    menu_sprites: Res<MainMenuAssets>
){
    let (width, height) = (BACKGROUND_SPRITE_SIZE.0, BACKGROUND_SPRITE_SIZE.1);
    let scale_x = window.width / width;
    let scale_y = window.height / height;

    commands.spawn(SpriteBundle {
        texture: menu_sprites.background_image.clone(),
        transform: Transform {
            translation: Vec3::new(0., 0., 1.),
            scale: Vec3::new(scale_x, scale_y, 1.),
            ..Default::default()
        },
        ..Default::default()
    });

    let button_text_style = TextStyle {
        font: menu_sprites.main_font.clone(),
        font_size: 20.0,
        color: BUTTON_TEXT_COLOR,
    };

    let button_style = Style {
        size: Size::new(Val::Px(250.0), Val::Px(65.0)),
        margin: UiRect::all(Val::Px(20.0)),
        justify_content: JustifyContent::Center,
        align_items: AlignItems::Center,
        ..default()
    };

    let spawn_button = |
        parent: &mut ChildBuilder,
        button_bkg: Handle<Image>,
        button_type: MenuButton,
        value: &str| {
        parent
            .spawn((
                ButtonBundle {
                    style: button_style.clone(),
                    image: UiImage::from(button_bkg),
                    ..default()
                },
                button_type,
            ))
            .with_children(|parent| {
                parent.spawn(TextBundle::from_section(
                    value,
                    button_text_style.clone()
                ));
            });
    };


    commands
        .spawn(NodeBundle {
            style: Style {
                margin: UiRect::all(Val::Auto),
                flex_direction: FlexDirection::Column,
                align_items: AlignItems::Center,
                ..Default::default()
            },
            ..Default::default()
        })
        .with_children(|parent| {
            spawn_button(parent,
                         menu_sprites.selected_button_image.clone(),
                         MenuButton::StartGame,
                         "START_GAME")
        })
        .with_children(|parent| {
            spawn_button(parent,
                         menu_sprites.default_button_image.clone(),
                         MenuButton::Settings,
                         "SETTINGS")
        })
        .with_children(|parent| {
            spawn_button(parent,
                         menu_sprites.default_button_image.clone(),
                         MenuButton::Exit,
                         "EXIT")
        })
        .insert(OnMainMenuScreen);


}

fn main_menu_screen_update(
    menu_assets: Res<MainMenuAssets>,
    mut interaction_query: Query<
        (&Interaction, &mut UiImage),
        (Changed<Interaction>, With<Button>)>) {

    for (interaction, mut bkg_image) in &mut interaction_query {

        *bkg_image = match interaction {
            Interaction::Clicked => {
                UiImage::from(menu_assets.selected_button_image_pressed.clone())
            },
            Interaction::Hovered => {
                UiImage::from(menu_assets.default_button_image_hovered.clone())
            }
            Interaction::None => {
                UiImage::from(menu_assets.default_button_image.clone())
            }
        }
    }
}

