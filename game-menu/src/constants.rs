use bevy::prelude::Color;

pub const BACKGROUND_COLOR_BLUE: Color = Color::rgb(13., 59., 102.);
pub const BACKGROUND_COLOR_WHITE: Color = Color::rgb(250., 240., 202.);
pub const BACKGROUND_COLOR_YELLOW: Color = Color::rgb(244., 211., 94.);
pub const BACKGROUND_COLOR_ORANGE: Color = Color::rgb(238., 150., 75.);
pub const BACKGROUND_COLOR_RED: Color = Color::rgb(249., 87., 56.);
pub const BACKGROUND_COLOR_BLACK: Color = Color::rgb(0., 0., 0.);

pub const BUTTON_TEXT_COLOR: Color = Color::rgb(0., 0., 0.);

pub const WINDOW_WIDTH: f32 = 400.;
pub const WINDOW_HEIGHT: f32 = 700.;

pub const BACKGROUND_SPRITE: &str = "menu/backgrounds/dark_purple.png";
pub const BACKGROUND_SPRITE_SIZE: (f32, f32) = (256., 256.);

pub const LOADING_BACKGROUND_SPRITE: &str = "menu/backgrounds/blue.png";
pub const LOADING_BACKGROUND_SPRITE_SIZE: (f32, f32) = (256., 256.);

pub const MAIN_FONT: &str = "menu/fonts/kenvector_future.ttf";
pub const MAIN_THIN_FONT: &str = "menu/fonts/kenvector_future_thin.ttf";

pub const MAIN_BUTTON: &str = "menu/button/blue_button00.png";
pub const MAIN_BUTTON_PRESSED: &str = "menu/button/blue_button02.png";
pub const MAIN_BUTTON_HOVERED: &str = "menu/button/blue_button01.png";

pub const SELECTED_BUTTON: &str = "menu/button/yellow_button00.png";
pub const SELECTED_BUTTON_PRESSED: &str = "menu/button/yellow_button01.png";
pub const SELECTED_BUTTON_HOVERED: &str = "menu/button/yellow_button02.png";

