use bevy::app::{App, Plugin, StartupStage};
use bevy::prelude::{Commands, Res};
use crate::structs::{GameState, WindowSize};

pub struct GamePlugin;

impl Plugin for GamePlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system_to_stage(StartupStage::PostStartup, setup);
    }
}

fn setup(
    mut commands: Commands,
    window_size: Res<WindowSize>,
    // game_state: Res<GameState>,
){
    // if game_state.into_inner() == &GameState::Game {
    //     println!("Play game")
    // }
}