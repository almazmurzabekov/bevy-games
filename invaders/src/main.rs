mod player;
mod components;
mod enemy;
mod formation;

use std::collections::HashSet;
use bevy::math::Vec3Swizzles;
use bevy::prelude::*;
use bevy::sprite::collide_aabb::collide;
use crate::components::{Enemy, Explosion, ExplosionTimer, ExplosionToSpawn, FromEnemy, FromPlayer, Laser, Movable, Player, SpriteSize, Velocity};
use crate::enemy::EnemyPlugin;
use crate::player::PlayerPlugin;

const WINDOW_WIDTH: f32 = 598.0;
const WINDOW_HEIGHT: f32 = 676.0;

const MAX_ENEMY_COUNT: u32 = 3;
const MAX_FORMATION_MEMBERS_COUNT: u32 = 3;
const PLAYER_RESPAWN_DELAY: f64 = 3.;

const PLAYER_SPRITE_NAME: &str = "player_a_01.png";
const PLAYER_SPRITE_SIZE: (f32, f32) = (144.0, 75.);

const EXPLOSION_SPRITES_SHEET_NAME: &str = "explo_a_sheet.png";
const EXPLOSION_SPRITES_SHEET_LENGTH: usize = 16;

const PLAYER_LASER_SPRITE_NAME: &str = "laser_a_01.png";
const PLAYER_LASER_SPRITE_SIZE: (f32, f32) = (9.0, 54.);

const ENEMY_SPRITE_NAME: &str = "enemy_a_01.png";
const ENEMY_SPRITE_SIZE: (f32, f32) = (93.0, 84.);

const ENEMY_LASER_SPRITE_NAME: &str = "laser_b_01.png";
const ENEMY_LASER_SPRITE_SIZE: (f32, f32) = (17.0, 55.);

const SPRITE_SCALE: f32 = 0.5;

const TIME_STEP: f32 = 1. / 60.;
const BASE_SPEED: f32 = 500.;

#[derive(Resource)]
pub struct EnemyCount(u32);

#[derive(Resource, Debug)]
pub struct PlayerState {
    alive: bool,
    killed_time: Option<f64>
}

impl PlayerState {
    pub fn kill(&mut self, timestamp: f64) {
        self.alive = false;
        self.killed_time = Some(timestamp);
    }

    pub fn respawn(&mut self) {
        self.alive = true;
        self.killed_time = None;
    }
}

impl Default for PlayerState {
    fn default() -> Self {
        Self { alive: false, killed_time: None }
    }
}

#[derive(Resource)]
pub struct WindowSize {
    width: f32,
    height: f32,
}

#[derive(Resource)]
struct GameTextures {
    player: Handle<Image>,
    player_laser: Handle<Image>,
    enemy: Handle<Image>,
    enemy_laser: Handle<Image>,
    explosion: Handle<TextureAtlas>,
}


fn main() {
    App::new()
        .insert_resource(ClearColor(Color::rgb(0.04, 0.04, 0.04)))
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            window: WindowDescriptor {
                title: "Rust Invaders".to_string(),
                width: WINDOW_WIDTH,
                height: WINDOW_HEIGHT,
                ..Default::default()
            },
            ..Default::default()
        }))
        .add_startup_system(setup_system)
        .add_system(movement_system)
        .add_system(player_laser_hit_enemy_system)
        .add_system(enemy_laser_hit_player_system)
        .add_system(explosion_to_spawn_system)
        .add_system(explosion_animation_system)
        .add_plugin(PlayerPlugin)
        .add_plugin(EnemyPlugin)
        .run()
}

fn setup_system(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
    mut windows: ResMut<Windows>,
) {
    commands.spawn(Camera2dBundle::default());

    if let Some(window) = windows.get_primary_mut() {
        let (width, height) = (window.width(), window.height());
        window.set_position(MonitorSelection::Current, IVec2::new(1320, 0));

        commands.insert_resource(WindowSize { width, height });
    }

    let texture_handle = asset_server.load(EXPLOSION_SPRITES_SHEET_NAME);
    let explosion_atlas = TextureAtlas::from_grid(
        texture_handle, Vec2::new(64., 64.), 4, 4, None, None);
    let explosion = texture_atlases.add(explosion_atlas);

    let game_textures = GameTextures {
        player: asset_server.load(PLAYER_SPRITE_NAME),
        player_laser: asset_server.load(PLAYER_LASER_SPRITE_NAME),
        enemy: asset_server.load(ENEMY_SPRITE_NAME),
        enemy_laser: asset_server.load(ENEMY_LASER_SPRITE_NAME),
        explosion,
    };

    commands.insert_resource(game_textures);
    commands.insert_resource(EnemyCount(0));
}

fn movement_system(
    mut commands: Commands,
    window_size: Res<WindowSize>,
    mut query: Query<(Entity, &Velocity, &mut Transform, &Movable)>,
) {
    for (entity, velocity, mut transform, movable) in query.iter_mut() {
        let translation = &mut transform.translation;
        translation.x += velocity.x * BASE_SPEED * TIME_STEP;
        translation.y += velocity.y * BASE_SPEED * TIME_STEP;

        if movable.auto_despawn {
            const MARGIN: f32 = 200.;
            if translation.y > window_size.height / 2.0 + MARGIN ||
                translation.x > window_size.width / 2.0 + MARGIN ||
                translation.y < -window_size.height / 2.0 - MARGIN ||
                translation.x < -window_size.width / 2.0 - MARGIN {
                commands.entity(entity).despawn();
            }
        }
    }
}

fn player_laser_hit_enemy_system(
    mut commands: Commands,
    mut enemy_count: ResMut<EnemyCount>,
    player_lasers: Query<(Entity, &Transform, &SpriteSize), (With<Laser>, With<FromPlayer>)>,
    enemies: Query<(Entity, &Transform, &SpriteSize), With<Enemy>>,
) {
    let mut despawned_entities: HashSet<Entity> = HashSet::new();
    for (laser_entity, laser_tf, laser_size) in player_lasers.iter() {
        if despawned_entities.contains(&laser_entity) {
            continue;
        }

        let laser_scale = Vec2::from(laser_tf.scale.xy());

        for (enemy_entity, enemy_tf, enemy_size) in enemies.iter() {
            if despawned_entities.contains(&enemy_entity)
                || despawned_entities.contains(&laser_entity) {
                continue;
            }

            let enemy_scale = Vec2::from(enemy_tf.scale.xy());

            let collision = collide(
                laser_tf.translation,
                laser_size.0 * laser_scale,
                enemy_tf.translation,
                enemy_size.0 * enemy_scale,
            );

            if let Some(_) = collision {
                commands.entity(enemy_entity).despawn();
                commands.entity(laser_entity).despawn();

                despawned_entities.insert(enemy_entity);
                despawned_entities.insert(laser_entity);

                commands.spawn(ExplosionToSpawn(enemy_tf.translation.clone()));
                enemy_count.0 -= 1;
            }
        }
    }
}

fn enemy_laser_hit_player_system(
    mut commands: Commands,
    enemy_lasers: Query<(Entity, &Transform, &SpriteSize), (With<Laser>, With<FromEnemy>)>,
    player: Query<(Entity, &Transform, &SpriteSize), With<Player>>,
    mut player_state: ResMut<PlayerState>,
    time: Res<Time>,
) {
    if let Ok((entity, player_tf, player_size)) = player.get_single() {
        let player_scale = Vec2::from(player_tf.scale.xy());
        for (laser_entity, laser_tf, laser_size) in enemy_lasers.iter() {
            let laser_scale = Vec2::from(laser_tf.scale.xy());

            let collision = collide(
                laser_tf.translation,
                laser_size.0 * laser_scale,
                player_tf.translation,
                player_size.0 * player_scale,
            );

            if let Some(_) = collision {
                commands.entity(entity).despawn();
                commands.entity(laser_entity).despawn();
                player_state.kill(time.elapsed_seconds_f64());

                commands.spawn(ExplosionToSpawn(player_tf.translation.clone()));
                break;
            }
        }
    }
}

fn explosion_to_spawn_system(
    mut commands: Commands,
    game_texture: Res<GameTextures>,
    explosion_spawns: Query<(Entity, &ExplosionToSpawn)>,
) {
    for (entity, explosion) in explosion_spawns.iter() {
        commands
            .spawn(SpriteSheetBundle {
                texture_atlas: game_texture.explosion.clone(),
                transform: Transform {
                    translation: explosion.0,
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(Explosion)
            .insert(ExplosionTimer::default());

        commands.entity(entity).despawn();
    }
}

fn explosion_animation_system(
    mut commands: Commands,
    time: Res<Time>,
    mut explosions: Query<(Entity, &mut ExplosionTimer, &mut TextureAtlasSprite), With<Explosion>>
) {
    for (entity, mut timer, mut sprite) in explosions.iter_mut() {
        timer.0.tick(time.delta());

        if timer.0.finished() {
            sprite.index += 1;
            if sprite.index >= EXPLOSION_SPRITES_SHEET_LENGTH {
                commands.entity(entity).despawn()
            }
        }
    }
}