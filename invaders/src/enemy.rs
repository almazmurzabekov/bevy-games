use std::f32::consts::PI;
use bevy::ecs::schedule::ShouldRun;
use bevy::prelude::*;
use bevy::time::FixedTimestep;
use rand::{Rng, thread_rng};
use crate::{ENEMY_LASER_SPRITE_SIZE, ENEMY_SPRITE_SIZE, EnemyCount, GameTextures, MAX_ENEMY_COUNT, SPRITE_SCALE, TIME_STEP, WindowSize};
use crate::components::{Enemy, FromEnemy, Laser, Movable, SpriteSize, Velocity};
use crate::formation::{Formation, FormationBuilder};

pub struct EnemyPlugin;

impl Plugin for EnemyPlugin {
    fn build(&self, app: &mut App) {
        app
            .insert_resource(FormationBuilder::default())
            .add_system_set(
                SystemSet::new()
                    .with_run_criteria(FixedTimestep::step(1.))
                    .with_system(spawn_enemy_system))
            .add_system_set(SystemSet::new()
                .with_run_criteria(enemy_fire_criteria)
                .with_system(enemy_fire_system)
            )
            .add_system(enemy_movement_system)
        ;
    }
}

fn spawn_enemy_system(
    mut commands: Commands,
    window_size: Res<WindowSize>,
    game_textures: Res<GameTextures>,
    mut enemy_count: ResMut<EnemyCount>,
    mut formation_builder: ResMut<FormationBuilder>
) {
    if enemy_count.0 < MAX_ENEMY_COUNT {
        let formation = formation_builder.build(&window_size);
        let (x, y) = formation.start;

        commands
            .spawn(SpriteBundle {
                texture: game_textures.enemy.clone(),
                transform: Transform {
                    translation: Vec3::new(x, y, 10.),
                    scale: Vec3::new(SPRITE_SCALE, SPRITE_SCALE, 1.),
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(Enemy)
            .insert(formation)
            .insert(SpriteSize::from(ENEMY_SPRITE_SIZE));

        enemy_count.0 += 1;
    }
}

fn enemy_fire_system(
    mut commands: Commands,
    enemies: Query<&Transform, With<Enemy>>,
    game_textures: Res<GameTextures>,
) {
    for transform in enemies.iter() {
        let (x, y) = (transform.translation.x, transform.translation.y);

        commands
            .spawn(SpriteBundle {
                texture: game_textures.enemy_laser.clone(),
                transform: Transform {
                    translation: Vec3::new(x, y - 15.0, 0.),
                    rotation: Quat::from_rotation_x(180.),
                    scale: Vec3::new(SPRITE_SCALE, SPRITE_SCALE, 1.)
                },
                ..Default::default()
            })
            .insert(Movable { auto_despawn: true })
            .insert(Velocity{ x: 0.0, y: -1.0 })
            .insert(Laser)
            .insert(SpriteSize::from(ENEMY_LASER_SPRITE_SIZE))
            .insert(FromEnemy);
    }

}

fn enemy_fire_criteria() -> ShouldRun {
    if thread_rng().gen_bool(1. / 60.) {
        ShouldRun::Yes
    } else {
        ShouldRun::No
    }
}

fn enemy_movement_system(mut enemies: Query<(&mut Transform, &mut Formation), With<Enemy>>) {
    for (mut enemy_tf, mut formation) in enemies.iter_mut() {
        let (org_x, org_y) = (enemy_tf.translation.x, enemy_tf.translation.y);

        let max_dst = TIME_STEP * formation.speed;
        let direction = if formation.start.0 < 0. { 1. } else { -1. };
        let (pivot_x, pivot_y) = formation.pivot;
        let (radius_x, radius_y) = formation.radius;

        let angle = formation.angle + direction * formation.speed * TIME_STEP / (radius_x.min
        (radius_y) * PI / 2.);

        let x_dest = radius_x * angle.cos() + pivot_x;
        let y_dest = radius_y * angle.sin() + pivot_y;

        let dx = org_x - x_dest;
        let dy = org_y - y_dest;

        let dst = (dx * dx + dy * dy).sqrt();
        let ratio = if dst != 0. {
            max_dst / dst
        } else {
            0.
        };

        let x = org_x - dx * ratio;
        let x = if dx > 0. { x.max(x_dest) } else { x.min(x_dest) };

        let y = org_y - dy * ratio;
        let y = if dy > 0. { y.max(y_dest) } else { y.min(y_dest) };

        if dst < max_dst * formation.speed / 20. {
            formation.angle = angle;
        }

        enemy_tf.translation.x = x;
        enemy_tf.translation.y = y;
    }
}