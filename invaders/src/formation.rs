use bevy::prelude::*;
use rand::{Rng, thread_rng};
use crate::{BASE_SPEED, MAX_FORMATION_MEMBERS_COUNT, WindowSize};

#[derive(Clone, Component)]
pub struct Formation {
    pub start: (f32, f32),
    pub radius: (f32, f32),
    pub pivot: (f32, f32),
    pub speed: f32,
    pub angle: f32
}

#[derive(Default, Resource)]
pub struct FormationBuilder {
    current_template: Option<Formation>,
    current_members: u32
}

impl FormationBuilder {
    pub fn build(&mut self, window_size: &WindowSize) -> Formation {
        match (&self.current_template, self.current_members > MAX_FORMATION_MEMBERS_COUNT) {
            (Some(tmpl), false) => {
                self.current_members += 1;
                tmpl.clone()
            },
            (None, _) | (_, true) => {
                let mut rng = thread_rng();

                // compute the start x/y
                let w_span = window_size.width / 2. + 100.;
                let h_span = window_size.height / 2. + 100.;
                let x = if rng.gen_bool(0.5) { w_span } else { -w_span };
                let y = rng.gen_range(-h_span..h_span) as f32;
                let start = (x, y);

                // compute the pivot x/y
                let w_span = window_size.width / 4.;
                let h_span = window_size.height / 3. - 50.;
                let pivot = (rng.gen_range(-w_span..w_span), rng.gen_range(0.0..h_span));

                // compute the radius
                let radius = (rng.gen_range(80.0..150.), 100.);

                // compute the start angle
                let angle = (y - pivot.1).atan2(x - pivot.0);

                // speed (fixed for now)
                let speed = BASE_SPEED;

                // create the formation
                let formation = Formation {
                    start,
                    radius,
                    pivot,
                    speed,
                    angle,
                };

                // store as template
                self.current_template = Some(formation.clone());
                // reset members to 1
                self.current_members = 1;

                formation
            }
        }
    }
}