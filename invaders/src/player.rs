use bevy::prelude::*;
use bevy::time::FixedTimestep;
use crate::{GameTextures, WindowSize, PLAYER_SPRITE_SIZE, SPRITE_SCALE, PLAYER_LASER_SPRITE_SIZE, PlayerState, PLAYER_RESPAWN_DELAY};
use crate::components::{FromPlayer, Laser, Movable, Player, SpriteSize, Velocity};


pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app
            .add_system_set(SystemSet::new()
                .with_run_criteria(FixedTimestep::step(0.5))
                .with_system(player_spawn_system))
            .add_system(player_fire_system)
            .add_system(player_keyboard_system)
            .insert_resource(PlayerState::default());
    }
}

fn player_spawn_system(
    mut commands: Commands,
    mut player_state: ResMut<PlayerState>,
    game_textures: Res<GameTextures>,
    window_size: Res<WindowSize>,
    time: Res<Time>,
) {
    let now = time.elapsed_seconds_f64();
    let last_shot = player_state.killed_time.unwrap_or(-1.);

    if !player_state.alive && (last_shot == -1. || now > last_shot + PLAYER_RESPAWN_DELAY) {
        let bottom = -window_size.height / 2.;
        commands
            .spawn(SpriteBundle {
                texture: game_textures.player.clone(),
                transform: Transform {
                    translation: Vec3::new(0., bottom + PLAYER_SPRITE_SIZE.1 / 2.0 * SPRITE_SCALE + 5.0, 10.),
                    scale: Vec3::new(SPRITE_SCALE, SPRITE_SCALE, 1.0),
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(Player)
            .insert(SpriteSize::from(PLAYER_SPRITE_SIZE))
            .insert(Velocity { x: 0., y: 0. })
            .insert(Movable { auto_despawn: false });

        player_state.respawn();
    }
}

fn player_fire_system(
    mut commands: Commands,
    kb: Res<Input<KeyCode>>,
    game_textures: Res<GameTextures>,
    query: Query<&Transform, With<Player>>,
) {
    if let Ok(player_tf) = query.get_single() {
        if kb.just_pressed(KeyCode::Space) {
            let x_offset: f32 = PLAYER_SPRITE_SIZE.0 / 2. * SPRITE_SCALE - 5.0 ;
            let x = player_tf.translation.x;
            let y = player_tf.translation.y;

            let mut spawn_laser_sprite = |x_offset| {
                commands
                    .spawn(SpriteBundle {
                        transform: Transform {
                            translation: Vec3::new(x + x_offset, y + 15., 0.0),
                            scale: Vec3::new(SPRITE_SCALE, SPRITE_SCALE, 1.0),
                            ..Default::default()
                        },
                        texture: game_textures.player_laser.clone(),
                        ..Default::default()
                    })
                    .insert(Velocity { x: 0.0, y: 1.0 })
                    .insert(Movable { auto_despawn: true })
                    .insert(FromPlayer)
                    .insert(Laser)
                    .insert(SpriteSize::from(PLAYER_LASER_SPRITE_SIZE))
                ;
            };

            spawn_laser_sprite(x_offset);
            spawn_laser_sprite(-x_offset);
        }
    }
}

fn player_keyboard_system(
    kb: Res<Input<KeyCode>>,
    mut player: Query<&mut Velocity, With<Player>>) {
    if let Ok(mut velocity) = player.get_single_mut() {
        velocity.x = if kb.pressed(KeyCode::Left) {
            -1.0
        } else if kb.pressed(KeyCode::Right) {
            1.0
        } else {
            0.0
        }
    }
}