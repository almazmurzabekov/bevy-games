use bevy::app::{App, StartupStage};
use bevy::math::Vec3;
use bevy::prelude::{Commands, Component, Plugin, Res, Transform};
use bevy::sprite::SpriteBundle;
use crate::constants::{BALL_HEIGHT, BALL_SPRITE_SIZE, BALL_WIDTH, STICK_HEIGHT};
use crate::structs::{GameTextures, Velocity, WindowSize};

#[derive(Component)]
pub struct BallComponent;

#[derive(Component, Debug)]
pub struct BallState {
    pub attached_to_player: bool,
}

pub struct Ball;

impl Plugin for Ball {
    fn build(&self, app: &mut App) {
        app
            .add_startup_system_to_stage(StartupStage::PostStartup, setup);
    }
}

fn setup(
    mut commands: Commands,
    game_textures: Res<GameTextures>,
    window_size: Res<WindowSize>
) {
    let (width, height) = BALL_SPRITE_SIZE;
    let scale_x = BALL_WIDTH / width;
    let scale_y = BALL_HEIGHT / height;

    let pos_x = 0.0;
    let pos_y = -window_size.height / 2. + STICK_HEIGHT + BALL_HEIGHT / 2.;

    commands
        .spawn(SpriteBundle {
            transform: Transform {
                translation: Vec3::new(pos_x, pos_y, 11.),
                scale: Vec3::new(scale_x, scale_y, 1.),
                ..Default::default()
            },
            texture: game_textures.ball_image.clone(),
            ..Default::default()
        })
        .insert(BallComponent)
        .insert(BallState { attached_to_player: true })
        .insert(Velocity { x: 0.0, y: 0.0 });
}