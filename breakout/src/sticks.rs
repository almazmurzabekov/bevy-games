use bevy::app::{App, Plugin, StartupStage};
use bevy::math::Vec3;
use bevy::prelude::{Commands, Component, Res, Transform};
use bevy::sprite::SpriteBundle;
use crate::constants::*;
use crate::structs::{GameTextures, WindowSize};

pub struct SticksGroup;

impl Plugin for SticksGroup {
    fn build(&self, app: &mut App) {
        app.add_startup_system_to_stage(StartupStage::PostStartup, spawn_sticks_group);
    }
}

#[derive(Component)]
pub struct Stick;

fn spawn_sticks_group(
    mut commands: Commands,
    game_textures: Res<GameTextures>,
    window_size: Res<WindowSize>
){
    let (width, height) = STICK_SPRITE_SIZE;
    let scale_x = STICK_WIDTH / width;
    let scale_y = STICK_HEIGHT / height;

    let mut position_x = MARGIN_TOP - window_size.width / 2.;
    let mut position_y = window_size.height / 2. - MARGIN_LEFT;

    let step_size_x = STICK_WIDTH + STICKS_MARGIN_WIDTH;
    let step_size_y = STICK_HEIGHT + STICKS_MARGIN_HEIGHT;

    for _ in 1..=STICKS_GROUP_HEIGHT {
        for _ in 1..=STICKS_GROUP_WIDTH {
            commands
                .spawn(SpriteBundle {
                    transform: Transform {
                        translation: Vec3::new(position_x, position_y, 10.),
                        scale: Vec3::new(scale_x, scale_y, 0.),
                        ..Default::default()
                    },
                    texture: game_textures.stick_image.clone(),
                    ..Default::default()
                })
                .insert(Stick);
            position_x += step_size_x;
        }
        position_y -= step_size_y;
        position_x = MARGIN_TOP - window_size.width / 2.;
    }
}