use bevy::asset::Handle;
use bevy::prelude::{Component, Image, Resource};


#[derive(Resource, Default)]
pub struct Scoreboard {
    pub score: u64
}

#[derive(Resource, Default)]
pub struct GameTextures {
    pub stick_image: Handle<Image>,
    pub paddle_image: Handle<Image>,
    pub ball_image: Handle<Image>,
}

#[derive(Resource)]
pub struct WindowSize {
    pub width: f32,
    pub height: f32,
}

impl WindowSize {
    pub fn from(width: f32, height: f32) -> Self {
        Self { width, height }
    }
}

#[derive(Component, Debug)]
pub struct Velocity {
    pub x: f32,
    pub y: f32,
}
