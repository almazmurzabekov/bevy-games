mod constants;
mod structs;
mod sticks;
mod player;
mod ball;

use bevy::prelude::*;
use crate::ball::{Ball, BallComponent, BallState};
use crate::constants::{BALL_SPRITE_NAME, BASE_SPEED, PADDLE_SPRITE_NAME, STICK_SPRITE_NAME, TIME_STEP, WINDOW_HEIGHT, WINDOW_WIDTH};
use crate::player::{Player, PlayerComponent};
use crate::sticks::SticksGroup;
use crate::structs::{GameTextures, Scoreboard, Velocity, WindowSize};

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(
            WindowPlugin {
                window: WindowDescriptor {
                    width: WINDOW_WIDTH,
                    height: WINDOW_HEIGHT,
                    monitor: MonitorSelection::Current,
                    title: "Breakout New".to_string(),
                    mode: WindowMode::Windowed,
                    resizable: false,
                    ..Default::default()
                },
                ..Default::default()
            }))
        .add_startup_system(setup)
        .add_plugin(SticksGroup)
        .add_plugin(Player)
        .add_plugin(Ball)
        .add_system(movement_system)
        .run();
}

fn setup(
    mut commands: Commands,
    mut asset_loader: Res<AssetServer>,
    mut windows: ResMut<Windows>
){
    commands.spawn(Camera2dBundle::default());
    commands.insert_resource(Scoreboard::default());

    if let Some(window) = windows.get_primary_mut() {
        window.set_position(MonitorSelection::Current, IVec2::new(1120, 0))
    }

    let game_textures = GameTextures {
        stick_image: asset_loader.load(STICK_SPRITE_NAME),
        paddle_image: asset_loader.load(PADDLE_SPRITE_NAME),
        ball_image: asset_loader.load(BALL_SPRITE_NAME),
    };
    commands.insert_resource(game_textures);
    commands.insert_resource(WindowSize::from(WINDOW_WIDTH, WINDOW_HEIGHT));
}


pub fn player_movement_system(
    mut player: Query<(&Velocity, &mut Transform), With<PlayerComponent>>) -> Option<(f32, f32)> {
    if let Ok((velocity, mut transform)) = player.get_single_mut() {
        let translation = &mut transform.translation;
        translation.x += velocity.x * BASE_SPEED * TIME_STEP;
        translation.y += velocity.y * BASE_SPEED * TIME_STEP;

        Some((translation.x, translation.y))
    } else {
        None
    }
}

pub fn movement_system(
    window_size: Res<WindowSize>,
    mut player: Query<(&Velocity, &mut Transform), With<PlayerComponent>>,
    mut ball: Query<(&Velocity, &mut Transform, &BallState),
        (With<BallComponent>, Without<PlayerComponent>)>,
) {
    let player_position = player_movement_system(player);

    if let Ok((velocity, mut transform, ball_state)) = ball.get_single_mut() {
        if ball_state.attached_to_player {
            if let Some((x, _)) = player_position {
                transform.translation.x = x
            }
        } else {
            let translation = &mut transform.translation;
            translation.x += velocity.x * BASE_SPEED * TIME_STEP;
            translation.y += velocity.y * BASE_SPEED * TIME_STEP;
        }
    }
}
