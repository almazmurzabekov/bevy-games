use bevy::app::{App, Plugin, StartupStage};
use bevy::math::Vec3;
use bevy::prelude::{Commands, Component, Entity, Input, KeyCode, Query, Res, Transform, With, Without};
use bevy::sprite::SpriteBundle;
use crate::ball::{BallComponent, BallState};
use crate::constants::{BASE_SPEED, PADDLE_HEIGHT, PADDLE_SPRITE_SIZE, PADDLE_WIDTH, TIME_STEP};
use crate::structs::{GameTextures, Velocity, WindowSize};

#[derive(Component)]
pub struct PlayerComponent;

pub struct Player;

impl Plugin for Player {
    fn build(&self, app: &mut App) {
        app
            .add_startup_system_to_stage(StartupStage::PostStartup, setup)
            .add_system(player_input_system);
    }
}

pub fn setup(
    mut commands: Commands,
    window_size: Res<WindowSize>,
    game_textures: Res<GameTextures>) {
    let pos_x = 0.;
    let pos_y = 0. - window_size.height / 2. + PADDLE_HEIGHT;

    let (width, height) = PADDLE_SPRITE_SIZE;
    let scale_x = PADDLE_WIDTH / width;
    let scale_y = PADDLE_HEIGHT / height;

    commands
        .spawn(SpriteBundle {
            texture: game_textures.paddle_image.clone(),
            transform: Transform {
                translation: Vec3::new(pos_x, pos_y, 10.),
                scale: Vec3::new(scale_x, scale_y, 0.),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(PlayerComponent)
        .insert(Velocity { x: 0.0, y: 0.0 });
}

pub fn player_input_system(
    input: Res<Input<KeyCode>>,
    mut player: Query<&mut Velocity, With<PlayerComponent>>,
    mut ball_query: Query<
        (&mut BallState, &mut Velocity),
        (With<BallComponent>, Without<PlayerComponent>)>
){
    match input.get_pressed().next() {
        Some(KeyCode::Left) => {
            if let Ok(mut velocity) = player.get_single_mut() {
                velocity.x =-1.0
            }
        },
        Some(KeyCode::Right) => {
            if let Ok(mut velocity) = player.get_single_mut() {
                velocity.x = 1.0
            }
        },
        Some(KeyCode::Space) => {
            if let Ok((mut ball_state, mut velocity)) = ball_query.get_single_mut() {
                if ball_state.attached_to_player {
                    ball_state.attached_to_player = false;
                    velocity.x = 1.0;
                    velocity.y = 1.0;
                }
            }
        }
        _ => {
            if let Ok(mut velocity) = player.get_single_mut() {
                velocity.x = 0.0
            }
        }
    }
}