pub const WINDOW_WIDTH: f32 = 800.;
pub const WINDOW_HEIGHT: f32 = 500.;

pub const STICK_SPRITE_NAME: &str = "stick.png";
pub const STICK_SPRITE_SIZE: (f32, f32) = (190., 49.);

pub const BALL_SPRITE_NAME: &str = "ball.png";
pub const BALL_SPRITE_SIZE: (f32, f32) = (36., 36.);
pub const BALL_WIDTH: f32 = 24.;
pub const BALL_HEIGHT: f32 = 24.;

pub const PADDLE_SPRITE_NAME: &str = "paddle.png";
pub const PADDLE_SPRITE_SIZE: (f32, f32) = (190., 45.);
pub const PADDLE_WIDTH: f32 = 140.;
pub const PADDLE_HEIGHT: f32 = 20.;

pub const STICKS_GROUP_WIDTH: usize = 9;
pub const STICKS_GROUP_HEIGHT: usize = 5;

pub const STICK_WIDTH: f32 = 80.;
pub const STICK_HEIGHT: f32 = 30.;
pub const STICKS_MARGIN_WIDTH: f32 = 5.;
pub const STICKS_MARGIN_HEIGHT: f32 = 5.;

pub const MARGIN_TOP: f32 = 60.;
pub const MARGIN_LEFT: f32 = 40.;

pub const BASE_SPEED: f32 = 700.;
pub const TIME_STEP: f32 = 1./60.;
